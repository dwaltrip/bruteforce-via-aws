var fs = require('fs');
var path = require('path');
var { calcNumPossibilities } = require('./mass-passphrase-generator');

const AVG_TRIES_PER_HOUR = (8 * Math.pow(10,5));

if (require.main === module) {
  var passphraseSpecFilename = process.argv[2];
  if (!passphraseSpecFilename || passphraseSpecFilename.length < 1) {
    usage('Invalid filename')
  }
  try {
    var spec = readJsonFile(passphraseSpecFilename);
  } catch (e) {
    console.error('Could not read json file:', passphraseSpecFilename);
    process.exit(1);
  }

  var num = calcNumPossibilities(spec.passphraseParts);
  var prefixMultiplier = spec.passphrasePrefixes ? spec.passphrasePrefixes.length : 1;
  num *= prefixMultiplier;
  var estimateOfHours = num / AVG_TRIES_PER_HOUR;

  var instanceCounts = [5,10,20,100];

  console.log('--------------------------------');
  console.log(' possibilities:', num.toLocaleString());
  console.log(' estimated compute time:', prettifyNumHours(estimateOfHours));
  instanceCounts.forEach(num => {
    var adjustedEstimate = estimateOfHours / num;
    console.log(` using ${num} instances:\t${prettifyNumHours(adjustedEstimate)}`);
  });
  console.log('--------------------------------');
}

function usage(msg) {
  var selfName = path.basename(__filename);
  console.error(msg);
  console.error(`Usage: node ${selfName} path/to/passphrase-spec.json`);
  process.exit(1);
}

function readJsonFile(filePath) {
  return JSON.parse(fs.readFileSync(filePath, 'utf8'));
}

function prettifyNumHours(numHours) {
  const mins = ((numHours - Math.floor(numHours)) * 60).toFixed(1);
  const hours = Math.floor(numHours);
  if (hours > 0) {
    return `${hours} hour${pluralize(hours)}, ${mins} minutes`
  } else {
    return `${mins} minutes`
  }
}

function pluralize(num) {
  return num > 1 ? 's' : '';
}
