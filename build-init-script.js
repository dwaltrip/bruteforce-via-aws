var fs = require('fs');
var path = require('path');
var execSync = require('child_process').execSync;

var { calcNumPossibilities } = require('./mass-passphrase-generator');

// TODO: record run history, and check it to make sure we are doing a new run
// const RUN_HISTORY_FILENAME = path.join(__dirname, 'run-history.txt');

const initScriptsDir = path.join(__dirname, 'full-init-scripts');
const logServerConfigFile = path.join(__dirname, 'logging/configs/config.json');

const CONFIG_PLACEHOLDER = '--CONFIG_PLACEHOLDER--';
const COMMIT_MSG_PLACEHOLDER = '--COMMIT_MSG_PLACEHOLDER--';
const CREATED_DATE_PLACEHOLDER = '--CREATED_DATE_PLACEHOLDER--';
const PASSPHRASE_CONFIG_FILENAME_PLACEHOLDER = '--PASSPHRASE_CONFIG_FILENAME_PLACEHOLDER--';
const NUM_POSSIBILITIES_PLACEHOLDER = '--NUM_POSSIBILITIES_PLACEHOLDER--';

// TODO: more error checking and validation that everything is what we expected?
if (require.main === module) {
  var configFilename = process.argv[2];
  var numSlices = parseInt(process.argv[3]);
  var runName = process.argv[4];

  checkDetails(notEmptyString(configFilename), 'First arg must be the config filename');
  checkDetails(numSlices && !isNaN(numSlices), 'Second arg must be the number of slices');
  checkDetails(notEmptyString(runName), 'Third arg must be the unique run name');

  var logServerConfig = readJsonFile(logServerConfigFile)
  var logServerURL = logServerConfig.LOG_SERVER_URL;
  var logServerUsername = logServerConfig.LOG_SERVER_USERNAME;
  var logServerPassword = logServerConfig.LOG_SERVER_PASSWORD;
  checkDetails(notEmptyString(logServerURL), 'LOG_SERVER_URL is required');
  checkDetails(notEmptyString(logServerUsername), 'LOG_SERVER_USERNAME is required');
  checkDetails(notEmptyString(logServerPassword), 'LOG_SERVER_PASSWORD is required');

  checkDetails(!isGitDirDirty(), 'Git working directory is dirty. Clean up before building an init-script.');
  var masterHash        = getHashForRef('master');
  var headHash          = getHashForRef('HEAD');
  var originMasterHash  = getHashForRef('origin/master');
  checkDetails(masterHash === headHash, 'master branch not synced with HEAD');
  checkDetails(masterHash === originMasterHash, 'master branch not synced with origin/master');

  // TODO: init-script file name should reflect source config file?
  var initScriptName = path.join(initScriptsDir, runName);
  checkDetails(!fs.existsSync(initScriptName), 'There is an existing init-script with this name');

  var passphraseConfig = readJsonFile(path.join(__dirname, configFilename));
  validatePassphraseSpec(passphraseConfig);

  var numPossibilities = calcNumPossibilities(passphraseConfig.passphraseParts);
  if (passphraseConfig.passphrasePrefixes) {
    numPossibilities *= passphraseConfig.passphrasePrefixes.length;
  }
  if ((numPossibilities / numSlices) === Math.floor(numPossibilities / numSlices)) {
    var perSliceCount = (numPossibilities / numSlices).toLocaleString();
  } else {
    var perSliceCount = [
      Math.floor(numPossibilities / numSlices).toLocaleString(), 'or',
      Math.ceil(numPossibilities / numSlices).toLocaleString()
    ].join(' ');
  }
  console.log('----------------------------------------------');
  console.log('--- Total number of possibilities:', numPossibilities.toLocaleString());
  console.log('--- Number of slices:', numSlices);
  console.log('--- Possibilities per slice:', perSliceCount);
  console.log('----------------------------------------------');

  var bashLines = [
    `run_name="${runName}"`,
    `init_script_git_hash="${masterHash}"`,
    `passphrase_config='${JSON.stringify(passphraseConfig, null, 2)}'`, // multi-line bash strings use single quotes
    `num_slices="${numSlices}"`,
    `log_server_url="${logServerURL}"`,
    `log_server_username="${logServerUsername}"`,
    `log_server_password="${logServerPassword}"`,
  ];

  fs.readFile(path.join(__dirname, 'init-script-template'), 'utf8', function (err, fileStr) {
    if (err) {
      return console.log(err);
    }

    assert(exactlyOneMatch(fileStr, CONFIG_PLACEHOLDER), 'CONFIG_PLACEHOLDER should occur exactly once');
    assert(exactlyOneMatch(fileStr, COMMIT_MSG_PLACEHOLDER), 'COMMIT_MSG_PLACEHOLDER should occur exactly once');
    assert(exactlyOneMatch(fileStr, CREATED_DATE_PLACEHOLDER), 'CREATED_DATE_PLACEHOLDER should occur exactly once');
    assert(exactlyOneMatch(fileStr, PASSPHRASE_CONFIG_FILENAME_PLACEHOLDER),
      'PASSPHRASE_CONFIG_FILENAME_PLACEHOLDER should occur exactly once');
    assert(exactlyOneMatch(fileStr, NUM_POSSIBILITIES_PLACEHOLDER),
      'NUM_POSSIBILITIES_PLACEHOLDER should occur exactly once');

    var updatedFileStr = fileStr
      .replace(CONFIG_PLACEHOLDER, bashLines.join('\n'))
      .replace(COMMIT_MSG_PLACEHOLDER, lastCommitMsgSubject())
      .replace(CREATED_DATE_PLACEHOLDER, (new Date()).toString())
      .replace(PASSPHRASE_CONFIG_FILENAME_PLACEHOLDER, configFilename)
      .replace(NUM_POSSIBILITIES_PLACEHOLDER,
        `${numPossibilities.toLocaleString()} (${perSliceCount} per slice)`)
    ;

    var file = fs.createWriteStream(initScriptName, { flags: 'w' });
    file.on('error', err => console.error('Error writing to file! -- ' + err));
    file.write(updatedFileStr);
    file.end();
    console.log('--- Built init script successfully!')
    console.log(`--- Run name: ${runName}`);
    console.log(`--- Filename of init-script: ${initScriptName}`);
    console.log('----------------------------------------------');
  });
}

function assert(condition, message, ...extraInfo) {
  if (!condition) {
    var extra = extraInfo && extraInfo.length > 0 ?
      '\n' + extraInfo.join('\n') :
      '';
    throw new Error(message + extra);
  }
}

function checkDetails(condition, message) {
  var border = Array(message.length + 3).join('-');
  if (!condition) {
    console.log(border);
    console.log(' ' + 'Invalid or incomplete parameters!');
    console.log(' ' + message);
    console.log(border);
    process.exit(1);
  }
}

// This should be in the mass-passphrase-generator repo...
function validatePassphraseSpec(spec) {
  var validKeys = ['passphraseParts', 'passphrasePrefixes', 'targetAddresses', 'Notes and comments'];
  var validTypes = ['CHOOSE_ONE', 'INTEGER_RANGE', 'FIXED'];
  var typeParams = {
    CHOOSE_ONE: ['options'],
    INTEGER_RANGE: ['start', 'end'],
    FIXED: ['value']
  };

  Object.keys(spec).forEach(key => {
    assert(validKeys.indexOf(key) >= 0, `Invalid key: ${key}`);
  });

  assert(spec.passphraseParts, 'Must specify `passphraseParts`');
  spec.passphraseParts.forEach(part => {
    assert(validTypes.indexOf(part.type) >= 0, `Invalid part type: ${part.type}`);
    var params = Object.keys(part).filter(param => param !== 'type');
    var requiredParams = typeParams[part.type];
    assert(areArraysEqual(params, requiredParams), `Incorrect params: ${params}`);

    if (part.type === 'CHOOSE_ONE') {
      assert(Array.isArray(part.options), '`options` for CHOOSE_ONE must be an array',
        `part.options: ${part.options}`);
      assert(hasNoDupes(part.options), 'CHOOSE_ONE parts should have no dupes in `options`',
        `part.options: ${part.options}`);
    } else if (part.type === 'FIXED') {
      assert(notEmptyString(part.value), 'FIXED `value` should be a non-empty string', `Recieved: ${part.value}`);
    } else if (part.type === 'INTEGER_RANGE') {
      var { start, end } = part;
      assert(!isNaN(start) && start >=0, 'INTEGER_RANGE `start` should be an integer that is >= 0',
        `start: ${start}`);
      assert(!isNaN(end) && start < end, 'INTEGER_RANGE `end` should be an integer larger than `start`',
        `end: ${end} -- start: ${start}`)
    }
  });

  assert(spec.targetAddresses, 'Must specify `targetAddresses`');
  assert(Array.isArray(spec.targetAddresses) && hasNoDupes(spec.targetAddresses),
    '`targetAddresses` must be an array with no dupes');

  if (spec.passphrasePrefixes) {
    var prefixes = spec.passphrasePrefixes;
    assert(Array.isArray(prefixes) && hasNoDupes(prefixes),
    '`passphrasePrefixes` must be an array with no dupes');
  }
}

function readJsonFile(filePath) {
  return JSON.parse(fs.readFileSync(filePath, 'utf8'));
}

function getHashForRef(ref) {
  return execSync(`git rev-parse --short ${ref}`, { cwd: __dirname }).toString().trim();
}

// returns true for any staged or unstaged changes, as well as untracked files
function isGitDirDirty() {
  var statusLines = parseInt(
    execSync(`echo $(git status --porcelain | wc -l)`, { cwd: __dirname }).toString().trim()
  );
  assert(!isNaN(statusLines), 'Git dirty check is broken');
  return statusLines > 0;
}

function lastCommitMsgSubject() {
  return execSync(`git log master -1 --pretty=%s`, { cwd: __dirname }).toString().trim();
}

function exactlyOneMatch(str, target) {
  return (str.match(new RegExp(target, 'g')) || []).length === 1;
}

function notEmptyString(str) {
  return str && typeof str === 'string' && str.length > 0;
}

// compare arrays of simple, sortable data types
function areArraysEqual(a1, a2) {
  a1 = a1.slice(); a1.sort();
  a2 = a2.slice(); a2.sort();
  return a1.filter((item, i) => item !== a2[i]).length === 0;
}

function hasNoDupes(ary) {
  var seen = {};
  for(let i=0; i<ary.length; i++) {
    if (ary[i] in seen) {
      return false;
    }
    seen[ary[i]] = true;
  }
  return true;
}
