var fs = require('fs');
var path = require('path');
var { generateSlice } = require('./mass-passphrase-generator');

function main() {
  var numSlices   = parseInt(process.argv[2]);
  var targetSlice = parseInt(process.argv[3]);
  var specFilename = process.argv[4];
  // var destFile    = process.argv[4];
  // if (!validateArgs(numSlices, targetSlice, destFile)) {
  if (!validateArgs(numSlices, targetSlice)) {
    console.log('--- Invalid args. Aborting...');
    return; 
  }
  var spec = getPassphraseSpec(specFilename);
  var parts = spec.passphraseParts;
  console.log('--- Starting generation slice of possibile passphrase --',
    `numSlices: ${numSlices}, targetSlice: ${targetSlice}, specFilename: ${specFilename}`
  );

  var slice = generateSlice(parts, numSlices, targetSlice);
  var midpoint = Math.ceil(slice.length / 2);

  var fileNameThread1 = 'tmp/passphrases-thread1.txt';
  var linesForThread1 = slice.slice(0, midpoint);

  var fileNameThread2 = 'tmp/passphrases-thread2.txt';
  var linesForThread2 = slice.slice(midpoint);

  writeFile(linesForThread1, fileNameThread1);
  writeFile(linesForThread2, fileNameThread2);
  writeFile(spec.targetAddresses, 'tmp/target-addresses.txt');
  if (spec.passphrasePrefixes) {
    writeFile(spec.passphrasePrefixes, 'tmp/passphrase-prefixes.txt');
  }
}

if (require.main === module) {
  main();
}

function getPassphraseSpec(specFilename) {
 if (!fs.existsSync(specFilename)) {
    throw new Error(`getPassphraseParts -- file '${specFilename}' not found!`);
  }
  var spec = readJsonFile(specFilename);
  if (!spec || (!spec.passphraseParts || !spec.targetAddresses)) {
    var specStr = JSON.stringify(spec, null, 2);
    throw new Error(`getPassphraseParts -- invalid passphrase specification:\n${specStr}\n`);
  }
  return spec;
};

function writeFile(lines, fileName) {
  var fileName = path.join(__dirname, fileName);
  var file = fs.createWriteStream(fileName, { flags: 'w' });
  file.on('error', err => console.error('Error writing to file! -- ' + err));
  lines.forEach(line => file.write(line + '\n'));
  file.end();
  console.log(`--- writeFile was successful for file '${fileName}'`);
}

// function validateArgs(numSlices, targetSlice, destFile) {
function validateArgs(numSlices, targetSlice) {
  if (!numSlices || isNaN(numSlices) || numSlices < 1) {
    console.log('--- The 1st argument, "numSlices", should be a positive integer. Received:',
      JSON.stringify(numSlices));
    return false;
  }
  if (isNaN(targetSlice) || targetSlice < 0) {
    console.log('--- The 2nd argument, "targetSlice", should be an integer that is >= 0. Received:',
      JSON.stringify(targetSlice));
    return false;
  }
  // if (!destFile || (destFile || '').trim().length === 0) {
  //   console.log('The 3rd, "destFile", should be a valid file name');
  //   return false;
  // }
  return true;
}

function readJsonFile(filePath) {
  return JSON.parse(fs.readFileSync(filePath, 'utf8'));
}
